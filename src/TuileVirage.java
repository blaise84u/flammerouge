
public class TuileVirage extends Tuile {
	
	boolean typevirage;
	
	public TuileVirage(int p ,int nbc, char nT,boolean type)
		{
			super(p,nbc,nT);
			this.typevirage=type;
		}
	
	public boolean estLeger(){
		boolean valide=false;
		if(this.typevirage==false)
			{
				valide=true;
			}
		return valide;
		}
	
	public boolean estSerre()
		{
			boolean valide=false;
			if(this.typevirage==true)
				{
					valide=true;
				}
			return valide;
		}
	
	

}
