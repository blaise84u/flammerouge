import java.util.*;


public abstract class Tuile {
	
	int pente;
	int nombreDeCases;
	char nomTuile;
	ArrayList<Tuile> listeTuiles;
	
	public Tuile(int p ,int nbc, char nT )
		{
			this.pente=p;
			this.nombreDeCases=nbc;
			this.nomTuile=nT;
		}
	
	public boolean estLigneDroite(Tuile t){
		boolean valide=false;
		if(t instanceof TuileLigneDroite)
			{
				valide=true;
			}
		return valide;
		}
	
	public boolean estMontee()
		{
			boolean valide=false;
			if(this.pente>0)
				{
					valide=true;
				}
			return valide;
		}
	
	public boolean estDescennte()
		{
			boolean valide=false;
			if(this.pente<0)
				{
					valide=true;
				}
			return valide;
		}
	
	
	
	
}
