public class TuileLigneDroite extends Tuile {
	
	public TuileLigneDroite(int p ,int nbc, char nT)
		{
			super(p, nbc, nT);
		}
	
	public boolean estLigneDepart()
		{
			boolean valide=false;
			if(this.nomTuile=='a')
				{
					valide=true;
				}
			return valide;
		
		}
	
	public boolean estLigneArrivee()
		{
			boolean valide=false;
			if(this.nomTuile=='u')
				{
					valide=true;
				}
			return valide;
			}
	
	public boolean estLigneNormale()
		{
			boolean valide=false;
			if(this.estLigneDepart()==false && this.estLigneArrivee()==false)
				{
					valide=true;
				}
			return valide;
		}
	

}
