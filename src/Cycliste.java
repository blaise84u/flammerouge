import java.util.*;


public abstract class Cycliste {
	
	private int positionActuelle;
	private boolean fileDroite;
	Carte carteChoisie;
	ArrayList<Carte> listeCartesDefausses;
	ArrayList<Carte> listeCartesFaceCachee;
	
	public Cycliste(int pos, boolean droite)
		{
			this.positionActuelle=pos;
			this.fileDroite=droite;
			
		}
	
	private ArrayList<Carte> piocher4Cartes(){
		ArrayList<Carte> cartes=new ArrayList<Carte>();
		for(int i=0;i<4;i++)
			{
				int max=this.listeCartesFaceCachee.size();
				int random=(int)(Math.random()*(max-0));
				cartes.add(listeCartesFaceCachee.get(random));
				listeCartesFaceCachee.remove(random);
			}
		return cartes;
		
	}
	
	public void choisirCarte(ArrayList<Carte> liste)
		{
			int max=liste.size();
			for(int i=0;i<max;i++)
				{
					Carte c=liste.get(i);
					System.out.println(c);
				}
			Scanner sc=new Scanner(System.in);
			int res=sc.nextInt();
			this.carteChoisie=liste.get(res);
			liste.remove(res);
			max=liste.size();
			for(int i=0;i<max;i++)
				{
					this.listeCartesDefausses.add(liste.get(i));
				}
			
			sc.close();
		}
	
	public void avancer(Carte car,Circuit cir)// A REVOIR //
		{
			this.positionActuelle=this.positionActuelle+car.numero;
			
		}
	
}
